module gitlab.com/sotatek-andrew/go-ecommerce-app

go 1.15

require (
	github.com/google/wire v0.5.0
	github.com/gorilla/mux v1.8.0
	github.com/gorilla/schema v1.2.0
	go.mongodb.org/mongo-driver v1.5.1
)
