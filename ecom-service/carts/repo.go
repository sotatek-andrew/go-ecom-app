package carts

import (
	mMongo "gitlab.com/sotatek-andrew/go-ecommerce-app/ecom-service/mongo"
	"go.mongodb.org/mongo-driver/mongo"
)

type CartsRepo struct {
	*mMongo.BaseRepo
}

func NewCartsRepo(db *mongo.Database) *CartsRepo {
	return &CartsRepo{
		&mMongo.BaseRepo{
			Collection: db.Collection("carts"),
		},
	}
}
