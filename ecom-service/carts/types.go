package carts

import (
	"time"

	"gitlab.com/sotatek-andrew/go-ecommerce-app/ecom-service/items"
)

const (
	CART_ACTIVE    = "ACTIVE"
	CART_PURCHASED = "PURCHASED"
)

type CreateCartDto struct {
	UserId string           `json:"userId" bson:"userId"`
	Items  map[string]int32 `json:"items" bson:"items"`
}

type MongoCreateCartData struct {
	CreateCartDto `bson:",inline"`
	Status        string    `json:"status" bson:"status"`
	Amount        float64   `json:"amount" bson:"amount"`
	CreatedAt     time.Time `json:"createdAt" bson:"createdAt"`
	UpdatedAt     time.Time `json:"updatedAt" bson:"updatedAt"`
}

type UpdateCartItemDto struct {
	ItemId   string `json:"itemId"`
	Quantity int32  `json:"quantity"`
}

type CartDocument struct {
	ID                  string `json:"id" bson:"_id"`
	MongoCreateCartData `bson:",inline"`
}

type FullCartDocument struct {
	ID        string                      `json:"id"`
	Status    string                      `json:"status" bson:"status"`
	Amount    float64                     `json:"amount" bson:"amount"`
	Items     map[string]FullCartItemData `json:"items" bson:"items"`
	CreatedAt time.Time                   `json:"createdAt" bson:"createdAt"`
	UpdatedAt time.Time                   `json:"updatedAt" bson:"updatedAt"`
}

func (d *CartDocument) ConvertToFullCartData(itemArr []items.ItemDocument) (cD *FullCartDocument) {
	cD = &FullCartDocument{
		ID: d.ID, Status: d.Status, Amount: d.Amount, CreatedAt: d.CreatedAt, UpdatedAt: d.UpdatedAt,
		Items: map[string]FullCartItemData{},
	}
	for _, item := range itemArr {
		cD.Items[item.ID] = FullCartItemData{
			Quantity: d.Items[item.ID],
			Item:     item,
		}
	}
	return
}

type FullCartItemData struct {
	Quantity int32              `json:"quantity"`
	Item     items.ItemDocument `json:"item"`
}

type ReadCartOptions struct {
	IsFull bool
}
