package carts

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"gitlab.com/sotatek-andrew/go-ecommerce-app/ecom-service/orders"
	"go.mongodb.org/mongo-driver/mongo"
)

type CartsAPI struct {
	queryDecoder *schema.Decoder
	cartService  *CartService
}

func NewCartsAPI(qD *schema.Decoder, cartService *CartService) *CartsAPI {
	return &CartsAPI{
		queryDecoder: qD,
		cartService:  cartService,
	}
}

func (api *CartsAPI) RegisterHandler(r *mux.Router) {
	log.Println("Register handlers for /carts api")
	r.HandleFunc("/carts/user", api.ReadUserActiveCart).Methods("GET")
	r.HandleFunc("/carts/{cartId}", api.ReadCartById).Methods("GET")
	r.HandleFunc("/carts", api.CreateNewCart).Methods("POST")
	r.HandleFunc("/carts/{cartId}", api.UpdateCartItem).Methods("PUT")
	r.HandleFunc("/carts/{cartId}/user", api.AssignCartToUser).Methods("PUT")
	r.HandleFunc("/carts/user/purchase", api.PurchaseCart).Methods("PUT")

}

func (api *CartsAPI) ReadCartById(w http.ResponseWriter, r *http.Request) {
	var cartId = mux.Vars(r)["cartId"]
	if cartId == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Bad request. Cart ID cannot be empty"))
		return
	}
	_, cartResponse, err := api.cartService.ReadCart(cartId, ReadCartOptions{IsFull: true})
	if err != nil {
		if err == mongo.ErrNoDocuments {
			w.WriteHeader(http.StatusNotFound)
			w.Write([]byte("Cannot find the cart with that ID"))
		} else {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("Something wrong while reading the cart"))
		}
		return
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(cartResponse)
}

func (api *CartsAPI) ReadUserActiveCart(w http.ResponseWriter, r *http.Request) {
	var userId = r.Header.Get("Authorization")
	if userId == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Cart ID nor user ID should be empty"))
		return
	}
	_, fullCart, err := api.cartService.GetUserActiveCart(userId, ReadCartOptions{IsFull: true})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something wrong while reading this user's active cart"))
		return
	}
	w.WriteHeader(http.StatusAccepted)
	json.NewEncoder(w).Encode(fullCart)
}

func (api *CartsAPI) CreateNewCart(w http.ResponseWriter, r *http.Request) {
	var createCartData = &CreateCartDto{}
	err := json.NewDecoder(r.Body).Decode(&createCartData)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Error while decoding the body"))
		return
	}
	newCart, err := api.cartService.CreateNewCart(createCartData)
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(newCart)
}

func (api *CartsAPI) AssignCartToUser(w http.ResponseWriter, r *http.Request) {
	cartId := mux.Vars(r)["cartId"]
	userId := r.Header.Get("Authorization")
	if userId == "" {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Unauthorized request"))
		return
	}
	if cartId == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Cart ID cannot be empty"))
		return
	}
	err := api.cartService.AssignCartToUser(cartId, userId)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something wrong while assign this cart to user"))
		return
	}
	w.WriteHeader(http.StatusAccepted)
	w.Write([]byte(fmt.Sprint("Successfully assign cart with id ", cartId, "to user with id", userId)))
}

func (api *CartsAPI) UpdateCartItem(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	cartId := params["cartId"]
	if cartId == "" {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Bad request. Cart ID cannot be empty"))
		return
	}
	updateCartItemData := UpdateCartItemDto{}
	err := json.NewDecoder(r.Body).Decode(&updateCartItemData)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Error while decoding the body"))
		return
	}
	err = api.cartService.UpdateCartItem(
		cartId,
		updateCartItemData.ItemId, updateCartItemData.Quantity,
	)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something wrong while updating the cart"))
		return
	}
	w.WriteHeader(http.StatusAccepted)
	w.Write([]byte(fmt.Sprint("Update cart with ID ", cartId, " successfully")))
}

func (api *CartsAPI) PurchaseCart(w http.ResponseWriter, r *http.Request) {
	userId := r.Header.Get("Authorization")
	if userId == "" {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("Unauthorized request"))
		return
	}
	billingInfo := &orders.OrderBillingInfo{}
	json.NewDecoder(r.Body).Decode(billingInfo)
	orderId, err := api.cartService.CreateOrderFromUserCart(userId, billingInfo)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Cannot create order from this user's cart"))
		return
	}
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(map[string]string{
		"orderId": orderId,
	})
}
