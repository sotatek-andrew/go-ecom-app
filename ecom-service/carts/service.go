package carts

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/sotatek-andrew/go-ecommerce-app/ecom-service/items"
	"gitlab.com/sotatek-andrew/go-ecommerce-app/ecom-service/orders"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type CartService struct {
	cartsRepo   *CartsRepo
	itemsRepo   *items.ItemsRepo
	ordersRepo  *orders.OrdersRepo
	orderSubMap orders.OrderSubscriptionChannelMap
}

func NewCartService(
	cartsRepo *CartsRepo,
	itemsRepo *items.ItemsRepo,
	ordersRepo *orders.OrdersRepo,
	orderSubMap orders.OrderSubscriptionChannelMap,
) *CartService {
	return &CartService{
		cartsRepo:   cartsRepo,
		itemsRepo:   itemsRepo,
		ordersRepo:  ordersRepo,
		orderSubMap: orderSubMap,
	}
}

func (s *CartService) GetUserActiveCart(
	userId string, opt ReadCartOptions,
) (cart *CartDocument, fullCart *FullCartDocument, err error) {
	cart = &CartDocument{}
	err = s.cartsRepo.FindOne(
		context.TODO(),
		bson.D{bson.E{Key: "userId", Value: userId}, bson.E{Key: "status", Value: CART_ACTIVE}},
		cart,
	)
	if opt.IsFull {
		itemDocs, _, e := s.CalculateCartItemsData(cart.Items)
		if e != nil {
			err = e
		}
		fullCart = cart.ConvertToFullCartData(itemDocs)
	}
	return
}

func (s *CartService) ReadCart(
	cartId string, opt ReadCartOptions,
) (cart *CartDocument, fullCart *FullCartDocument, err error) {
	cart = &CartDocument{}
	cartObjectId, err := primitive.ObjectIDFromHex(cartId)
	if err != nil {
		return
	}
	err = s.cartsRepo.FindOne(
		context.TODO(),
		bson.D{bson.E{Key: "_id", Value: cartObjectId}},
		&cart,
	)
	if opt.IsFull {
		itemDocs, _, e := s.CalculateCartItemsData(cart.Items)
		if e != nil {
			err = e
		}
		fullCart = cart.ConvertToFullCartData(itemDocs)
	}
	return
}

func (s *CartService) CreateNewCart(dto *CreateCartDto) (cart *CartDocument, err error) {
	data := MongoCreateCartData{
		CreateCartDto: *dto,
	}
	// populate timestamp
	currentTimeUtc := time.Now().UTC()
	data.CreatedAt = currentTimeUtc
	data.UpdatedAt = currentTimeUtc
	// new cart default status is ACTIVE
	data.Status = CART_ACTIVE
	// find the list of item and calculate total amount
	_, data.Amount, err = s.CalculateCartItemsData(data.Items)
	if err != nil {
		return
	}
	cartId, err := s.cartsRepo.Create(context.TODO(), data)
	if err != nil {
		return
	}
	cart = &CartDocument{
		MongoCreateCartData: data,
		ID:                  cartId,
	}
	return
}

func (s *CartService) CalculateCartItemsData(
	cartItemsMap map[string]int32,
) (itemArr []items.ItemDocument, totalAmount float64, err error) {
	itemArr = []items.ItemDocument{}
	itemObjIdArr := make([]primitive.ObjectID, len(cartItemsMap))
	itemArrIndex := 0
	for itemId := range cartItemsMap {
		itemObjIdArr[itemArrIndex], err = primitive.ObjectIDFromHex(itemId)
		if err != nil {
			return
		}
		itemArrIndex += 1
	}
	err = s.itemsRepo.Find(
		context.TODO(),
		bson.M{
			"_id": bson.M{
				"$in": itemObjIdArr,
			},
		},
		&itemArr,
	)
	totalAmount = float64(0)
	for _, cartItem := range itemArr {
		totalAmount += cartItem.Price * float64(cartItemsMap[cartItem.ID])
	}
	return
}

func (s *CartService) UpdateCartItem(
	cartId string,
	itemId string,
	quantity int32,
) (err error) {
	cart, _, err := s.ReadCart(cartId, ReadCartOptions{})
	if err != nil {
		return
	}
	item := &items.ItemDocument{}
	err = s.itemsRepo.FindById(context.TODO(), itemId, item)
	if err != nil {
		return
	}
	newAmount := cart.Amount - float64(cart.Items[itemId]-quantity)*item.Price
	updateKey := fmt.Sprint("items.", itemId)
	updateQuery := bson.M{}
	if quantity != 0 {
		updateQuery["$set"] = bson.M{
			"amount":  newAmount,
			updateKey: quantity,
		}
	} else {
		updateQuery["$set"] = bson.M{
			"amount": newAmount,
		}
		updateQuery["$unset"] = bson.M{
			updateKey: quantity,
		}
	}
	err = s.cartsRepo.UpdateByID(context.TODO(), cart.ID, updateQuery)
	return
}

func (s *CartService) AssignCartToUser(cartId, userId string) (err error) {
	cart, _, err := s.ReadCart(cartId, ReadCartOptions{IsFull: false})
	if cart.UserId != "" && cart.UserId != userId {
		err = errors.New("This cart is already owned")
		return
	}
	err = s.cartsRepo.UpdateByID(
		context.TODO(),
		cart.ID,
		bson.M{"$set": bson.M{"userId": userId}},
	)
	return
}

func (s *CartService) CreateOrderFromUserCart(
	userId string,
	billingInfo *orders.OrderBillingInfo,
) (orderId string, err error) {
	_, fullCart, err := s.GetUserActiveCart(userId, ReadCartOptions{IsFull: true})
	if err != nil {
		return
	}
	if fullCart.Status == CART_PURCHASED {
		err = errors.New("Attempt to purchase already purchased cart")
		return
	}
	data := &orders.MongoCreateOrderData{}
	dto := &orders.CreateOrderDto{
		UserId:   userId,
		ItemList: map[string]orders.OrderItemData{},
	}
	for itemId, itemData := range fullCart.Items {
		dto.ItemList[itemId] = orders.OrderItemData{
			Name:     itemData.Item.Name,
			Quantity: itemData.Quantity,
			Price:    itemData.Item.Price,
		}
	}
	// TODO: Replace this after connect with payment service
	dto.BillingInfo = *billingInfo
	data.CreateFromDto(dto)
	orderId, err = s.ordersRepo.Create(context.TODO(), data)
	// temp function to push order status notification to channel
	go func() {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()
		if s.orderSubMap[orderId] == nil {
			s.orderSubMap[orderId] = make(chan orders.OrderSubscriptionMessage, 6)
		}
		s.orderSubMap[orderId] <- orders.OrderSubscriptionMessage{
			Status:  orders.ORDER_CREATED,
			OrderID: orderId,
		}
		time.Sleep(time.Second * 60)
		err := s.ordersRepo.UpdateByID(ctx, orderId, bson.M{"$set": bson.M{"status": orders.ORDER_SHIPPING}})
		if err != nil {
			close(s.orderSubMap[orderId])
			return
		}
		s.orderSubMap[orderId] <- orders.OrderSubscriptionMessage{
			Status:  orders.ORDER_SHIPPING,
			OrderID: orderId,
		}
		s.ordersRepo.UpdateByID(ctx, orderId, bson.M{"$set": bson.M{"status": orders.ORDER_DELIVERED}})
		if err != nil {
			close(s.orderSubMap[orderId])
			return
		}
		s.orderSubMap[orderId] <- orders.OrderSubscriptionMessage{
			Status:  orders.ORDER_DELIVERED,
			OrderID: orderId,
		}
	}()
	s.cartsRepo.UpdateByID(context.TODO(), fullCart.ID, bson.M{
		"$set": bson.M{
			"status": CART_PURCHASED,
		},
	})
	return
}
