package mongo

import (
	"context"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

func NewMongoClient () *mongo.Client{
	log.Println("Start connecting to database");
	log.Println("Time-out 10 seconds");
	ctx, cancel := context.WithTimeout(context.Background(), 10 * time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI("mongodb://localhost:27017"))
	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal("Something wrong while connnecting the mongodb server")
		log.Panic(err)
	}
	defer cancel()
	return client
}