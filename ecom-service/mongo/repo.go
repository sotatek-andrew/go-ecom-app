package mongo

import (
	"context"
	"errors"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type BaseRepo struct {
	Collection *mongo.Collection
}

func (r *BaseRepo) Find(ctx context.Context, filter interface{}, results interface{}) (e error) {
	if e != nil {
		return
	}
	cursor, e := r.Collection.Find(ctx, filter)
	if e != nil {
		return
	}
	cursor.All(ctx, results)
	return
}

func (r *BaseRepo) FindById(ctx context.Context, id string, result interface{}) (e error) {
	objId, e :=  primitive.ObjectIDFromHex(id);
	if e != nil {
		return
	}
	e = r.Collection.FindOne(ctx, bson.D{bson.E{Key: "_id", Value: objId}}).Decode(result)
	return
}


func (r *BaseRepo) FindOne(ctx context.Context, filter interface{}, result interface{}) (e error) {
	e = r.Collection.FindOne(ctx, filter).Decode(result)
	if e != nil {
		result = nil
	}
	return
}

func (r *BaseRepo) Create(ctx context.Context, data interface{}) (newID string, e error) {
	resultDoc, e := r.Collection.InsertOne(ctx, data)
	if e != nil {
		return
	}
	if id, ok := resultDoc.InsertedID.(primitive.ObjectID); ok {
		newID = id.Hex()
		return
	}
	newID = ""
	return
}

func (r *BaseRepo) UpdateByID (ctx context.Context, id string, data interface{}) (e error){
	objId, e :=  primitive.ObjectIDFromHex(id);
	if e != nil {
		return
	}
	updateResult, e := r.Collection.UpdateByID(ctx, objId, data)
	if updateResult == nil || updateResult.MatchedCount == 0 {
		e = errors.New("ID don't match")
	}
	return
}
