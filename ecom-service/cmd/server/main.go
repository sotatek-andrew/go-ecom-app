// +build wireinject

package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/google/wire"
	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
	"gitlab.com/sotatek-andrew/go-ecommerce-app/ecom-service/carts"
	"gitlab.com/sotatek-andrew/go-ecommerce-app/ecom-service/items"
	mMongo "gitlab.com/sotatek-andrew/go-ecommerce-app/ecom-service/mongo"
	"gitlab.com/sotatek-andrew/go-ecommerce-app/ecom-service/orders"
	"go.mongodb.org/mongo-driver/mongo"
)

func main() {
	mongoClient := mMongo.NewMongoClient()
	api := SetUpAPI(mongoClient.Database("api-ecom"))
	api.Init()
	server := NewApp(api, mongoClient)
	server.Start()
}

type App struct {
	server      *http.Server
	mongoClient *mongo.Client
}

func NewApp(api *API, mongoClient *mongo.Client) *App {
	return &App{
		mongoClient: mongoClient,
		server: &http.Server{
			Addr:         "0.0.0.0:2802",
			WriteTimeout: time.Second * 15,
			ReadTimeout:  time.Second * 15,
			IdleTimeout:  time.Second * 60,
			Handler:      api.BaseRouter,
		},
	}
}

func (app *App) Start() {
	log.Println("Server will listen on " + app.server.Addr)
	go func() {
		if err := app.server.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()
	err := app.server.Shutdown(ctx)
	if err != nil {
		log.Fatal(err.Error())
	}
	if err = app.mongoClient.Disconnect(ctx); err != nil {
		log.Fatal(err.Error())
	}
	log.Println("Server is shutting down")
	os.Exit(0)
}

type API struct {
	BaseRouter *mux.Router
	ItemsAPI   *items.ItemsAPI
	CartsAPI   *carts.CartsAPI
	OrdersAPI  *orders.OrdersAPI
}

func NewAPI(
	itemsAPI *items.ItemsAPI,
	cartsAPI *carts.CartsAPI,
	ordersAPI *orders.OrdersAPI,
) *API {
	return &API{
		BaseRouter: mux.NewRouter().PathPrefix("/api/ecom").Subrouter(),
		ItemsAPI:   itemsAPI,
		CartsAPI:   cartsAPI,
		OrdersAPI:  ordersAPI,
	}
}

func SetUpAPI(db *mongo.Database) *API {
	wire.Build(
		NewAPI,
		items.NewItemsAPI,
		items.NewItemsRepo,
		carts.NewCartsRepo,
		carts.NewCartsAPI,
		carts.NewCartService,
		schema.NewDecoder,
		orders.NewOrdersAPI,
		orders.NewOrdersRepo,
		orders.NewSubscriptionChannelMap,
	)
	return &API{}
}

func (api *API) Init() {
	log.Println("Start registering handler")
	api.ItemsAPI.RegisterHandler(api.BaseRouter)
	api.CartsAPI.RegisterHandler(api.BaseRouter)
	api.OrdersAPI.RegisterHandler(api.BaseRouter)
}
