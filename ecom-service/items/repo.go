package items

import (
	mMongo "gitlab.com/sotatek-andrew/go-ecommerce-app/ecom-service/mongo"
	"go.mongodb.org/mongo-driver/mongo"
)

type ItemsRepo struct {
	*mMongo.BaseRepo
}

func NewItemsRepo(db *mongo.Database) *ItemsRepo{
	return &ItemsRepo{
		&mMongo.BaseRepo{
			Collection: db.Collection("items"),
		},
	}
}