package items

import (
	"time"

	"go.mongodb.org/mongo-driver/bson"
)

type CreateItemDto struct {
	Name string `bson:"name" json:"name"`
	Price float64 `bson:"value" json:"price"`

}

type MongoCreateItemData struct {
	CreateItemDto `bson:",inline"`
	CreatedAt time.Time `bson:"created_at" json:"createdAt"`
	UpdatedAt time.Time `bson:"updated_at" json:"updatedAt"`
}

type IndexItemsFilter struct {
	CreatedAtFrom time.Time `schema:"createdAtFrom"`
	CreatedAtTo time.Time	`schema:"createdAtTo"`
}


func (f *IndexItemsFilter) ConvertToMongoFilter() (bson.D, error) {
	mongoFilter := bson.D{}
	if !f.CreatedAtFrom.IsZero() || !f.CreatedAtTo.IsZero() {
		utcLocation, err := time.LoadLocation("UTC")
		if err != nil {
			return nil, err
		}
		createdAtFilter := bson.E{Key: "created_at", Value: bson.D{}}
		if filterValue, ok := createdAtFilter.Value.(bson.D); ok {
			if !f.CreatedAtFrom.IsZero() {
				filterValue = append(filterValue, bson.E{Key: "$gte", Value: f.CreatedAtFrom.In(utcLocation)})
			}
			if !f.CreatedAtTo.IsZero() {
				filterValue = append(filterValue, bson.E{Key: "$lte", Value: f.CreatedAtTo.In(utcLocation)})
			}
			createdAtFilter.Value = filterValue
		}
		mongoFilter = append(mongoFilter, createdAtFilter)
	}
	return mongoFilter, nil
}

type ItemDocument struct{
	ID string `json:"id" bson:"_id"`
	MongoCreateItemData `bson:",inline"`
}