package items

import (
	"context"
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/gorilla/schema"
)

type ItemsAPI struct{
	queryDecoder *schema.Decoder
	ItemsRepo *ItemsRepo
}

func NewItemsAPI(itemsRepo *ItemsRepo, queryDecoder *schema.Decoder) *ItemsAPI{
	return &ItemsAPI{
		ItemsRepo: itemsRepo,
		queryDecoder: queryDecoder,
	}
}

func (api *ItemsAPI) RegisterHandler(r *mux.Router){
	r.HandleFunc("/items", api.indexItems).Methods("GET")
	r.HandleFunc("/items", api.createNewItems).Methods("POST")
	log.Println("Register handlers for /items api");
}

func (api *ItemsAPI) indexItems(w http.ResponseWriter, r *http.Request){
	filter := IndexItemsFilter{}
	e := api.queryDecoder.Decode(&filter, r.URL.Query())
	if e != nil {
		log.Println(e.Error())
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Cannot decode the query params when indexing items"))
		return
	}
	mongoFilter, e := filter.ConvertToMongoFilter()
	if e != nil {
		log.Println(e.Error())
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Cannot decode the query params when indexing items"))
		return
	}
	docs := []ItemDocument{}
	e = api.ItemsRepo.Find(context.TODO(), mongoFilter, &docs)
	if e != nil {
		log.Println(e.Error())
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something wrong while fetching item data"))
		return
	}
	w.WriteHeader(http.StatusAccepted)
	json.NewEncoder(w).Encode(&docs)
}

func (api *ItemsAPI) createNewItems (w http.ResponseWriter, r *http.Request) {
	dto := CreateItemDto{}
	err := json.NewDecoder(r.Body).Decode(&dto)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Bad request body. Cannot decode into JSON data"))
		return
	}
	data := MongoCreateItemData{
		CreateItemDto: dto,
	}
	data.CreatedAt = time.Now().UTC()
	data.UpdatedAt = data.CreatedAt
	insertedId, err := api.ItemsRepo.Create(context.TODO(), data)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something is wrong while inserting document"))
		return
	}
	resultItem := ItemDocument{
		MongoCreateItemData: data,
		ID: insertedId,
	}
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(&resultItem)
}