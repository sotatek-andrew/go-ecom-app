package orders

import (
	mMongo "gitlab.com/sotatek-andrew/go-ecommerce-app/ecom-service/mongo"
	"go.mongodb.org/mongo-driver/mongo"
)

type OrdersRepo struct {
	*mMongo.BaseRepo
}

func NewOrdersRepo(db *mongo.Database) *OrdersRepo {
	return &OrdersRepo{
		&mMongo.BaseRepo{
			Collection: db.Collection("orders"),
		},
	}
}
