package orders

import "time"

const (
	ORDER_CREATED   = "CREATED"
	ORDER_CANCELLED = "CANCELLED"
	ORDER_SHIPPING  = "SHIPPING"
	ORDER_DELIVERED = "DELIVERED"
)

type CreateOrderDto struct {
	ItemList    map[string]OrderItemData `bson:"itemList"`
	UserId      string                   `bson:"userId"`
	BillingInfo OrderBillingInfo         `bson:"billingInfo"`
}

type OrderItemData struct {
	Name     string  `bson:"name"`
	Quantity int32   `bson:"quantity"`
	Price    float64 `bson:"price"`
}

type OrderBillingInfo struct {
	CardholderName  string `bson:"cardholderName" json:"cardholderName"`
	ShippingAddress string `bson:"shippingAddress" json:"shippingAddress"`
}

type MongoCreateOrderData struct {
	CreateOrderDto `bson:",inline"`
	Status         string    `bson:"status"`
	UpdatedAt      time.Time `bson:"createdAt"`
	CreatedAt      time.Time `bson:"updatedAt"`
}

func (mD *MongoCreateOrderData) CreateFromDto(dto *CreateOrderDto) {
	currentTime := time.Now().UTC()
	mD.CreateOrderDto = *dto
	mD.Status = ORDER_CREATED
	mD.CreatedAt, mD.UpdatedAt = currentTime, currentTime
}

type OrderDocument struct {
	MongoCreateOrderData `bson:",inline"`
	ID                   string `bson:"id" json:"id"`
}
