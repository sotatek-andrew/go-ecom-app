package orders

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
)

type OrdersAPI struct {
	ordersRepo *OrdersRepo
}

func NewOrdersAPI(ordersRepo *OrdersRepo) *OrdersAPI {
	return &OrdersAPI{
		ordersRepo: ordersRepo,
	}
}

func (api *OrdersAPI) RegisterHandler(r *mux.Router) {
	log.Println("Registering handlers for /orders api")
	r.HandleFunc("/orders/user", api.GetAllUserOrders).Methods("GET")
	r.HandleFunc("/orders/{orderID}/cancel", api.CancelOrder).Methods("PUT")
	r.HandleFunc("/orders/{orderID}/order-status-noti", api.SubscribeToOrderStatusNoti).Methods("PUT")
}

func (api *OrdersAPI) GetAllUserOrders(w http.ResponseWriter, r *http.Request) {
	userOrders := []OrderDocument{}
	userId := r.Header.Get("Authorization")
	if userId == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	err := api.ordersRepo.Find(r.Context(), bson.M{"userId": userId}, &userOrders)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something wrong while fetching user's orders"))
		return
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(userOrders)
}

func (api *OrdersAPI) CancelOrder(w http.ResponseWriter, r *http.Request) {
	order, err := api.CheckUserOrderValid(w, r)
	if err != nil {
		return
	}
	err = api.ordersRepo.UpdateByID(
		r.Context(),
		order.ID,
		bson.M{"$set": bson.M{"status": ORDER_CANCELLED}},
	)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something wrong while update user's order"))
		return
	}
	w.WriteHeader(http.StatusOK)
}

func (api *OrdersAPI) SubscribeToOrderStatusNoti(w http.ResponseWriter, r *http.Request) {
	order, err := api.CheckUserOrderValid(w, r)
	if err != nil {
		return
	}
	if order.Status == ORDER_DELIVERED || order.Status == ORDER_CANCELLED {
		return
	}
	if _, ok := w.(http.Flusher); ok {
		log.Printf(fmt.Sprint("Start subscribing to order with id ", order.ID))
		w.Header().Set("Content-Type", "text/event-stream")
		w.Header().Set("Cache-Control", "no-cache")
		w.Header().Set("Connection", "keep-alive")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		for {

		}
	}
	return
}

func (api *OrdersAPI) CheckUserOrderValid(w http.ResponseWriter, r *http.Request) (order *OrderDocument, err error) {
	orderId := mux.Vars(r)["orderID"]
	userId := r.Header.Get("Authorization")
	if userId == "" {
		w.WriteHeader(http.StatusUnauthorized)
		err = errors.New("Unauthorized request")
		return
	}
	order = &OrderDocument{}
	err = api.ordersRepo.FindById(r.Context(), orderId, order)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Something wrong while fetching user's orders"))
		return
	}
	if order.UserId != userId {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte("Forbidden to cancel this order"))
		err = errors.New("Forbidden access")
		return
	}
	return
}
