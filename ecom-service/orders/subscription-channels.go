package orders

type OrderSubscriptionMessage struct {
	OrderID string `json:"orderId"`
	Status  string `json:"status"`
}

type OrderSubscriptionChannelMap = map[string]chan OrderSubscriptionMessage

func NewSubscriptionChannelMap() map[string]chan OrderSubscriptionMessage {
	return map[string]chan OrderSubscriptionMessage{}
}
